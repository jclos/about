# About me

I am a graduate student in the [Robert Gordon University](https://www.rgu.ac.uk), working through a SICSA scholarship to research text mining, and more precisely the detection and exploitation of argumentation structures in user comments. I also work as a data scientist in [Cognitive Geology](https://www.cognitivegeology.com) a small but successful start-up based in Edinburgh. 

My interests revolve around the study of information and intelligence, from traditional information retrieval and social media mining, to the recent trends of deep learning. I am particularly attracted to the pluridisciplinarity of applied AI and to collaborating with multiple domains, from digital humanities to distributed intelligence.

